insert into STUDENT (id, student_number, forename, surname, date_of_birth,  year_of_study) values (1,'40191','John','Smith',TO_DATE('15/05/2008', 'DD/MM/YYYY'), 3);
insert into STUDENT (id, student_number, forename, surname, date_of_birth,  year_of_study) values (2,'40282','John','Clark',TO_DATE('13/04/2007', 'DD/MM/YYYY'), 1);
insert into STUDENT (id, student_number, forename, surname, date_of_birth,  year_of_study) values (3,'40373','John','Newman',TO_DATE('17/03/2007', 'DD/MM/YYYY'), 2);
insert into STUDENT (id, student_number, forename, surname, date_of_birth,  year_of_study) values (4,'40464','Mary','Whelan',TO_DATE('13/09/2006', 'DD/MM/YYYY'), 2);
insert into STUDENT (id, student_number, forename, surname, date_of_birth,  year_of_study) values (5,'40555','Mary','Newman',TO_DATE('18/04/2007', 'DD/MM/YYYY'), 1);
insert into STUDENT (id, student_number, forename, surname, date_of_birth,  year_of_study) values (6,'40646','James','Smith',TO_DATE('15/02/2005', 'DD/MM/YYYY'), 2);
insert into STUDENT (id, student_number, forename, surname, date_of_birth,  year_of_study) values (7,'40737','James','Duignan',TO_DATE('19/08/2009', 'DD/MM/YYYY'), 1);
insert into STUDENT (id, student_number, forename, surname, date_of_birth,  year_of_study) values (8,'40828','Anne','Macken',TO_DATE('23/03/2005', 'DD/MM/YYYY'), 1);
insert into STUDENT (id, student_number, forename, surname, date_of_birth,  year_of_study) values (9,'40919','Anne','Bloggs',TO_DATE('23/08/2005', 'DD/MM/YYYY'), 1);
insert into STUDENT (id, student_number, forename, surname, date_of_birth,  year_of_study) values (10,'40115','Anne','Newman',TO_DATE('26/03/2004', 'DD/MM/YYYY'), 2);
insert into STUDENT (id, student_number, forename, surname, date_of_birth,  year_of_study) values (11,'40215','Frank','Smith',TO_DATE('12/09/2006', 'DD/MM/YYYY'), 1);
insert into STUDENT (id, student_number, forename, surname, date_of_birth,  year_of_study) values (12,'40315','Noel','Glennon',TO_DATE('28/04/2007', 'DD/MM/YYYY'), 2);
insert into STUDENT (id, student_number, forename, surname, date_of_birth,  year_of_study) values (13,'40413','Andy','Clark',TO_DATE('29/02/2008', 'DD/MM/YYYY'), 1);

insert into MODULE (id, module_code, name, departement, semester, credits, pass_mark, pass_by_compensation) values (1, 'MH101', 'Statistics', 'Maths', 1, 5, 65, true);
insert into MODULE (id, module_code, name, departement, semester, credits, pass_mark, pass_by_compensation) values (2, 'MH102', 'Calculus', 'Maths', 1, 10, 45, true);
insert into MODULE (id, module_code, name, departement, semester, credits, pass_mark, pass_by_compensation) values (3, 'MH103', 'Geometery', 'Maths', 2, 5, 60, true);
insert into MODULE (id, module_code, name, departement, semester, credits, pass_mark, pass_by_compensation) values (4, 'MH104', 'Algebra', 'Maths', 2, 5, 65, true);
insert into MODULE (id, module_code, name, departement, semester, credits, pass_mark, pass_by_compensation) values (5, 'MH105', 'Number Theory', 'Maths', 2, 5, 40, true);
insert into MODULE (id, module_code, name, departement, semester, credits, pass_mark, pass_by_compensation) values (6, 'GE101', 'Urban Developement', 'Geography', 1, 10, 65, true);
insert into MODULE (id, module_code, name, departement, semester, credits, pass_mark, pass_by_compensation) values (7, 'GE102', 'Weather and Climate', 'Geography', 1, 5, 55, true);
insert into MODULE (id, module_code, name, departement, semester, credits, pass_mark, pass_by_compensation) values (8, 'GE103', 'Caves and Features', 'Geography', 2, 10, 60, true);
insert into MODULE (id, module_code, name, departement, semester, credits, pass_mark, pass_by_compensation) values (9, 'GE104', 'Natural Resources', 'Geography', 2, 10, 55, true);
insert into MODULE (id, module_code, name, departement, semester, credits, pass_mark, pass_by_compensation) values (10, 'CS102', 'Java', 'Computer_Science', 1, 10, 55, false);
insert into MODULE (id, module_code, name, departement, semester, credits, pass_mark, pass_by_compensation) values (11, 'CS103', 'Databases', 'Computer_Science', 1, 5, 65, false);
insert into MODULE (id, module_code, name, departement, semester, credits, pass_mark, pass_by_compensation) values (12, 'CS104', 'Cloud Computing', 'Computer_Science', 2, 10, 45, false);
insert into MODULE (id, module_code, name, departement, semester, credits, pass_mark, pass_by_compensation) values (13, 'CS105', 'Computer systems', 'Computer_Science', 2, 5, 40, false);

